+++
title = "Contact"
date = "2018-01-23T15:30:51-08:00"
page = "contact-page"
menu = "main"
weight = 2
+++

You can reach me at [ian@ianbrandt.com](mailto:ian@ianbrandt.com), or for Brandt Academy business at 
[ian@brandt.academy](mailto:ian@brandt.academy).

Friends and colleagues will find my phone number on
[Facebook](https://www.facebook.com/ianbrandt1 'Ian Brandt on Facebook') or 
[LinkedIn](http://www.linkedin.com/in/ianbrandt 'Ian Brandt on LinkedIn') respectively.
