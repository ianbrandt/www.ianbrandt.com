+++
title = "Home"
date = "2018-09-28T16:24:25-07:00"
page = "index-page"
menu = "main"
weight = 1
+++

Hello, World!
-------------

I’m the founder and executive director of 
[Brandt Academy](https://www.brandt.academy/ 'Brandt Academy – Forging STEM pioneers').  We're a non-profit {{< abbr 
title="Science, Technology, Engineering, and Mathematics" text="STEM" >}} academy where middle and high school students 
learn technology engineering from industry professionals.  I'm also the founder and organizer of the 
[San Diego Kotlin User Group](https://www.meetup.com/sd-kotlin/ 'SD Kotlin'), and serve on the board of directors as 
well as co-organize the [San Diego Java User's Group](http://www.sdjug.org/ 'SDJUG').

I have a passion for learning, mentoring, and servant leadership.  I've been a long-time proponent and practitioner of 
Agile, Lean, DevOps, and Software Craftsmanship values, principles, and practices.  I'm constantly seeking to refine my 
craft, and leverage my skills to foster high-performing technology delivery organizations.

I'm a member of the [Association for Computing Machinery](https://www.acm.org/ 'ACM').  I also enjoy contributing to 
open-source projects, and studying software, electronics, computer, mechanical, and robotics engineering.


History
-------

My fascination with technology began when I was young.  Ever curious, I wanted to learn how to make the 
early-generation video games my childhood friends and I would play.  I purchased an Intel 8088-based computer with a 
year’s worth of paper route savings, and started teaching myself how to program in BASIC and C from magazines and 
books.  My learning efforts greatly accelerated upon gaining access to the internet in the mid-90’s, and grew into a 
career in the lead-up to the Y2K problem.  Amateur and professional experience combined, I've now been programming 
computers for over 30 years.


Other Interests
---------------

Away from the keyboard I keep busy with an overly-long list of hobbies.  I love hiking, backpacking, rock climbing, 
and beach volleyball.  I enjoy [reading and listening to audiobooks](https://www.goodreads.com/ianbrandt 'Ian Brandt 
on Goodreads'), particularly on the topics of leadership, start-ups, business strategy, cosmology, evolution, 
neuroscience, nutrition, personal productivity and improvement, and personal finance.  I play upright and electric 
bass, am slowly learning the piano, and I like experimenting with hardware and software synthesizers.  Otherwise I’m 
often tinkering with cars, motorcycles, and radio control helicopters.  I love movies, and follow Formula 1, MotoGP, 
NFL football, and America's Cup sailing.  Some dormant interests I hope to revive include sailboat racing, playing the 
cello, and aviation.
