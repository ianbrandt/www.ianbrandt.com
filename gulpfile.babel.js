'use strict'

import _ from 'lodash'
import autoprefixer from 'autoprefixer'
import cleanCss from 'gulp-clean-css'
import del from 'del'
import gulp from 'gulp'
import gutil from 'gulp-util'
import imageMin from 'gulp-imagemin'
import newer from 'gulp-newer'
import postcss from 'gulp-postcss'
import rename from 'gulp-rename'
import rev from 'gulp-rev'
import revDel from 'rev-del'
import sass from 'gulp-sass'
import sassInheritance from 'gulp-sass-inheritance'
import sourcemaps from 'gulp-sourcemaps'
import {spawn, spawnSync} from 'child_process'
import tap from 'gulp-tap'

const sourcePath = 'src'
const stagePath = 'static'
const dataPath = 'data'
const publicPath = 'public'

const imageGlob = '*.{jpg,jpeg,png,svg,tif,tiff}'

const paths = {
	images: {
		src: sourcePath + '/img/**/' + imageGlob,
		dest: stagePath + '/img',
		watch: sourcePath + '/img/**/' + imageGlob
	},
	styles: {
		srcPath: sourcePath + '/sass',
		src: sourcePath + '/sass/**/*.{sass,scss}',
		dest: stagePath + '/css',
		concat: stagePath + '/css/main.css',
		manifestFile: dataPath + '/css/manifest.json'
	}
}

function buildImages () {

	return gulp.src(paths.images.src, {since: gulp.lastRun(buildImages)})
		.pipe(newer({
			dest: paths.images.dest
		}))
		.pipe(imageMin(
			[
				imageMin.svgo()
			],
			{
				verbose: true
			}))
		.pipe(gulp.dest(paths.images.dest))
}

function buildStyles () {

	return gulp.src(paths.styles.src, {since: gulp.lastRun(buildStyles)})
	//.pipe(newer(paths.styles.concat)) TODO: Map source to rev'ed version.
		.pipe(sassInheritance({dir: paths.styles.srcPath}))
		.pipe(sourcemaps.init())
		.pipe(sass({includePaths: paths.styles.srcPath}).on('error', sass.logError))
		.pipe(postcss([autoprefixer()]))
		.pipe(cleanCss())
		.pipe(rev())
		.pipe(rename({extname: '.min.css'}))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.styles.dest))
		.pipe(rev.manifest({path: paths.styles.manifestFile}))
		.pipe(revDel({
			oldManifest: paths.styles.manifestFile,
			dest: paths.styles.dest
		}))
		.pipe(tap(deleteOldSourceMaps))
		.pipe(gulp.dest('.'))
}

export const build = gulp.series(gulp.parallel(buildImages, buildStyles), function (cb) {

	let env = Object.create(process.env)
	env.HUGO_ENV = 'production'

	spawnSync('hugo', ['--cleanDestinationDir'], {
		env: env,
		stdio: 'inherit'
	})

	cb()
})

export const serve = gulp.series(build, function () {
	serveSite()
})

export const serveDrafts = gulp.series(build, function () {
	serveSite(true)
})

function serveSite (drafts = false) {

	gulp.watch(paths.images.watch, buildImages)
	gulp.watch(paths.styles.src, buildStyles)

	const hugoArgs = [
		'serve',
		'--bind=0.0.0.0',
		'--renderToDisk',
		'--cleanDestinationDir'
	]

	if (drafts) {
		hugoArgs.push('--buildDrafts', '--buildFuture')
	}

	spawn('hugo', hugoArgs, {stdio: 'inherit'})
}

/*
 * Clean
 */

function cleanData(cb) {

	del([paths.styles.manifestFile], cb)

	cb()
}

function cleanStatic(cb) {

	del([stagePath], cb)

	cb()
}

function cleanSite(cb) {

	del([publicPath], cb)

	cb()
}

export const clean = gulp.parallel(cleanData, cleanSite, cleanStatic)

// https://github.com/callumacrae/rev-del/issues/10
function deleteOldSourceMaps (tappedFile) {

	if (!_.isEmpty(tappedFile.revDeleted)) {

		const mapPaths = tappedFile.revDeleted.map(revDeletedPath => revDeletedPath + '.map')

		del(mapPaths).then(deletedMapPaths => {

			gutil.log(gutil.colors.red('Deleted map files:\n'), deletedMapPaths.join('\n'))
		})
	}
}
